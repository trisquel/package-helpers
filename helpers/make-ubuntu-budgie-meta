#!/bin/sh
#
#    Copyright (C) 2022  Luis Guzmán <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=4
. ./config

# Rebrand and tweak packages
sed -i 's|Ubuntu|Trisquel|g' debian/control
sed -i '/^Package:/s|ubuntu-budgie-desktop|trisquel-budgie-desktop|' debian/control
sed -i '/raspi/d' metapackage-map
sed -i '/budgie-desktop-raspi/,/it not be removed./d' debian/control

# Remove deprecated packages
grep -l ubuntu-drivers-common desktop*| xargs -r sed -i '/ubuntu-drivers-common/d'
grep -l snapd desktop*| xargs -r sed -i '/snapd/d'
grep -l whoopsie desktop* | xargs -r sed -i '/whoopsie/d'
grep -l apport desktop* | xargs -r sed -i '/apport/d'
grep -l ubuntu-budgie desktop*| xargs -r sed -i 's|ubuntu-budgie|trisquel-budgie|g'
sed -i 's|ubuntu-budgie|trisquel-budgie|' metapackage-map

# Add custom trisquel packages
grep -l thunderbird desktop* | xargs -r sed -i "/thunderbird/a abrowser"
grep -l thunderbird desktop* | xargs -r sed -i "s|thunderbird|icedove|"
grep -l gnome-software desktop* | xargs -r sed -i "s|gnome-software|trisquel-app-install|g"

replace 'ubuntu-release-upgrader-gtk' 'trisquel-release-upgrader-gtk'
replace 'fonts-ubuntu' 'fonts-trisquel'
# Remove raspimeta package components
find -name \*-raspi-\*|xargs rm
rm update.cfg

# Fix order names by sorting them out
for i in $(ls desktop-*)
do
cat $i | sort -o $i
done

changelog "Rebuild to drop non-available deprecated and non-free source packages."
package
