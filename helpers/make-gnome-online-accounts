#!/bin/sh
#
#    Copyright (C) 2019 Mason Hock <mason@masonhock.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=2

. ./config

# revert ubuntu/snap customizations
sed '/libsnapd-glib-dev/d' -i debian/control
sed '/libsnapd-glib-dev/d' -i debian/control.in
sed '/python3-macaroonbakery/d' -i debian/control
sed '/python3-macaroonbakery/d' -i debian/control.in
sed '/lpa_helper.py/d' -i debian/gnome-online-accounts.install
sed '/ubuntu_sso_provider/d' -i debian/libgoa-backend-1.0-1.symbols
rm debian/patches/0001-ubuntu-sso-provider.patch debian/patches/0002-livepatch-auth.patch
sed '/0001-ubuntu-sso-provider.patch/d' -i debian/patches/series
sed '/0002-livepatch-auth.patch/d' -i debian/patches/series
sed '/--enable-ubuntu-sso/d' -i debian/rules
sed 's/--enable-foursquare \\/--enable-foursquare/' -i debian/rules


## Avoid promote questionable online services via dconf,
## still available for users who enable them.
#https://help.gnome.org/admin/system-admin-guide/stable/lockdown-online-accounts.html.en
cat << EO-GOA > debian/gnome-online-accounts.postinst
#!/bin/sh
# Customize services via dconf
mkdir -p /etc/dconf/db/local.d/ /etc/dconf/db/local.db/locks

cat << EOF > /etc/dconf/profile/user
user-db:user
system-db:local
EOF

cat << EOF > /etc/dconf/db/local.d/00-goa
[org/gnome/online-accounts]
whitelisted-providers= ['kerberos', 'owncloud', 'imap_smtp']

EOF

cat << EOF > /etc/dconf/db/local.db/locks/goa
# Lock the list of providers that are allowed to be loaded
/org/gnome/online-accounts/whitelisted-providers
EOF

dconf update
EO-GOA

changelog "Remove Snap build dependency"

package

