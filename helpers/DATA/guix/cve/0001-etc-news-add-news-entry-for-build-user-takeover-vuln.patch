From 532996c5908fb14cc8d102865280fb203c075c9c Mon Sep 17 00:00:00 2001
From: Reepca Russelstein <reepca@russelstein.xyz>
Date: Sun, 20 Oct 2024 17:32:23 -0500
Subject: [PATCH] etc: news: add news entry for build user takeover
 vulnerability fix.

* etc/news.scm: add entry about build user takeover vulnerability.
---
 etc/news.scm | 32 ++++++++++++++++++++++++++++++++
 1 file changed, 32 insertions(+)

diff --git a/etc/news.scm b/etc/news.scm
index a90f92a9ff..3fb53a9849 100644
--- a/etc/news.scm
+++ b/etc/news.scm
@@ -33,6 +33,38 @@
 (channel-news
  (version 0)
 
+ (entry (commit "5966e0fdc78771c562e0f484a22f381a77908be0")
+        (title
+         (en "Daemon vulnerability allowing takeover of build users fixed"))
+        (body
+         (en "A vulnerability allowing a local user to execute arbitrary code
+as any of the build users has been identified and fixed.  Most notably, this
+allows any local user to alter the result of any local build, even if it
+happens inside a container.  The only requirements to exploit this
+vulnerability are the ability to start a derivation build and the ability to
+run arbitrary code with access to the store in the root PID namespace on the
+machine that build occurs on.  This largely limits the vulnerability to
+multi-user systems.
+
+This vulnerability is caused by the fact that @command{guix-daemon} does not
+change ownership and permissions on the outputs of failed builds when it moves
+them to the store, and is also caused by there being a window of time between
+when it moves outputs of successful builds to the store and when it changes
+their ownership and permissions.  Because of this, a build can create a binary
+with both setuid and setgid bits set and have it become visible to the outside
+world once the build ends.  At that point any process that can access the
+store can execute it and gain the build user's privileges.  From there any
+process owned by that build user can be manipulated via procfs and signals at
+will, allowing the attacker to control the output of its builds.
+
+You are advised to upgrade @command{guix-daemon}.  Run @command{info \"(guix)
+Upgrading Guix\"}, for info on how to do that.  Additionally, if there is any
+risk that a builder may have already created these setuid binaries (for
+example on accident), run @command{guix gc} to remove all failed build
+outputs.
+
+See @uref{https://issues.guix.gnu.org/73919} for more information on this
+vulnerability.")))
  (entry (commit "2161820ebbbab62a5ce76c9101ebaec54dc61586")
         (title
          (en "Risk of local privilege escalation during user account creation")
-- 
2.45.2

